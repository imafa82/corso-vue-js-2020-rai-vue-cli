export const filterMixin = {
    methods: {
        filterMixin: function (value, property, text) {
            return text && property?
                value.filter(el => el[property].toLowerCase().indexOf(text.toLowerCase()) !== -1)
                : value;
        }
    }
}
