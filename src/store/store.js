import Vue from 'vue';
import Vuex from 'vuex';
import auth from "./modules/auth/auth";
import users from "./modules/users/users";

Vue.use(Vuex);
export const store = new Vuex.Store({
    modules: {
        auth,
        users
    }
});
