const mutations = {
    SET_LOGGED_USER: (state, payload) => state.user = payload,
    SET_TOKEN: (state, payload) => state.token = payload,
    DELETE_TOKEN: (state) => state.token = undefined,
    DELETE_USER: (state) => state.user = undefined
}

export default mutations;
