import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";
import state from "./state";

const auth = {
    state,
    getters,
    actions,
    mutations
};

export default auth;
