import Vue from 'vue';
import router from '../../../router'
const actions = {
    autoLogin: (context) => {
        if(localStorage.getItem('token')){
            context.dispatch('setToken', localStorage.getItem('token'));
            context.dispatch('userLoggedCall');
        }
    },
    userLoggedCall: (context) => {
        Vue.http.get('https://www.massimilianosalerno.it/jwt/api/user').then(res => {
            let user = res && res.body ? res.body : undefined;
            context.dispatch('setLoggedUser', user);
        });
    },
    login: (context, payload) => {
        localStorage.setItem('token', payload);
        context.dispatch('setToken', payload);
    },
    logout: (context) => {
      localStorage.removeItem('token');
      context.commit('DELETE_USER');
      context.commit('DELETE_TOKEN');
      router.push({name: 'Login'});
    },
    setLoggedUser: (context, payload) => {
        context.commit('SET_LOGGED_USER', payload);
    },
    setToken: (context, payload) => {
        context.commit('SET_TOKEN', payload);
    }



};

export default actions;
