import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";
import state from "./state";

const users = {
    state,
    getters,
    actions,
    mutations
};

export default users;
