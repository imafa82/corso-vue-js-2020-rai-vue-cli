const mutations = {
    SET_USERS: (state, payload = []) => state.users = payload.map(el => ({...el})),
    ADD_USER: (state, payload) => state.users = [...state.users, {...payload}],
    EDIT_USER: (state, payload) => state.users = state.users.map(user => user.id === payload.id ? {...user, ...payload} : user ),
    REMOVE_USER: (state, payload) => state.users = state.users.filter(user => user.id !== payload),
    SET_NEW_USER: (state, payload = {}) => state.newUser = {...payload},
    RESET_NEW_USER: (state) => state.newUser = undefined,
}

export default mutations;
