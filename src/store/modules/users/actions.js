import Vue from 'vue';
const actions = {
    addInsertCall: (context, payload) => {
        if(payload.id){
            context.dispatch('editUserCall', payload);
        }else {
            context.dispatch('addUserCall', payload);
        }
    },
    addUserCall: (context, payload) => {
        Vue.http.post('https://jsonplaceholder.typicode.com/users', payload).then(res => {
            context.dispatch('addUser', {...payload, id: res.body.id});
            context.dispatch('resetNewUser');
        })
    },
    editUserCall: (context, payload) => {
        Vue.http.put('https://jsonplaceholder.typicode.com/users/' + payload.id, payload).then(res => {
            context.dispatch('editUser', payload);
            context.dispatch('resetNewUser');
        })
    },
    usersCall: (context) => {
        Vue.http.get('users/').then(res => {
            context.dispatch('setUsers', res && res.ok && res.body ? res.body: [])
        }, error => {
            context.dispatch('setUsersFail')
        });
    },
    deleteUserCall: (context, payload) => {
        Vue.http.delete(`https://jsonplaceholder.typicode.com/users/${payload}`).then(res => {
            this.$store.dispatch('removeUser', payload);
        }, err => {
            context.dispatch('setUsersFail');
        })
    },
    setUsersFail: (context) => {
        // qui potremmo andare poi a settare un array di errori e quindi visualizzare quell'array in pagina
      alert('errore in fase di chiamata');
    },
    setUsers: (context, payload) => {
        context.commit('SET_USERS', payload)
    },
    addUser: (context, payload) => {
        context.commit('ADD_USER', payload)
    },
    editUser: (context, payload) => {
        context.commit('EDIT_USER', payload)
    },
    removeUser: (context, payload) => {
        context.commit('REMOVE_USER', payload)
    },
    setNewUser: (context, payload) => {
        context.commit('SET_NEW_USER', payload)
    },
    resetNewUser: (context) => {
        context.commit('RESET_NEW_USER')
    },
};

export default actions;
