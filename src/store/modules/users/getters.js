const getters = {
    getUsers: state => state.users,
    getNewUser: state => state.newUser ? {...state.newUser} : undefined,
};

export default getters;
