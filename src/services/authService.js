import router from '../router';
const authService ={
    methods: {
        setLocalStorage(){
            if(localStorage.getItem('token')){
                this.$store.dispatch('setToken', localStorage.getItem('token'));
                this.setUser();
            }
        },
        setToken(token){
            this.$store.dispatch('setToken', token);
            localStorage.setItem('token', token);
        },
        setUser(){
            this.$http.get('https://www.massimilianosalerno.it/jwt/api/user').then(res => {
               let user = res && res.body ? res.body : undefined;
               this.$store.dispatch('setLoggedUser', user);
            });
        },
        logout(){
          localStorage.removeItem('token');
          router.push({name: 'Login'});
        }
    }
};

export default authService;
