export const get = (url) => call(url, {method: 'GET', headers: getHeaders()});
export const post = (url, data) => call(url, {method: 'GET', headers: getHeaders(), body: JSON.stringify(data)});
export const put = (url, data) => call(url, {method: 'GET', headers: getHeaders(), body: JSON.stringify(data)});
export const callDelete = (url) => call(url, {method: 'GET', headers: getHeaders()});

const call = async (url, obj) => {
    const res = await fetch(url, obj);
    if(!res.ok){
        if (res.status === 401){
            localStorage.removeItem('token');
            location.reload();
        }
        throw res;
    }
    const json = await res.json();
    return json;
    // return fetch(url, obj).then(res => {
    //     if(!res.ok){
    //         if (res.status === 401){
    //             localStorage.removeItem('token');
    //             location.reload();
    //         }
    //         throw res;
    //     }
    //     return res.json()
    // })
}

const getHeaders = () => {
    return {
        'Content-type': 'application/json',
        ...(localStorage.getItem('token')?
            {'Authorization': 'Bearer ' + localStorage.getItem('token')} : {})
    }
}
