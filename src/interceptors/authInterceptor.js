import {store} from '../store/store'

export default (request, next) => {
    if(localStorage.getItem('token')){
        request.headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    }
    next(response => {
        if(response.status === 401){
            store.dispatch('logout');
        }
    });
}
