import Home from '../views/auth/Home.vue'
import Page from "../views/auth/Page";
import User from "../views/auth/User";
import Users from "../views/auth/Users";
import Actors from "../views/auth/actors/Actors";
import ActorList from "../views/auth/actors/ActorList";
import ActorShow from "../views/auth/actors/ActorShow";
import ActorEdit from "../views/auth/actors/ActorEdit";
import Directive from "../views/auth/Directive";

const authRoutes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        menuTitle: 'Home'
    },
    {
        path: '/page',
        name: 'Page',
        component: Page,
        menuTitle: 'Stili dinamici'
    },
    {
        path: '/directive',
        name: 'Directive',
        component: Directive,
        menuTitle: 'Direttive'
    },
    {
        path: '/actors',
        name: 'Actors',
        component: Actors,
        menuTitle: 'Attori',
        children: [
            {
                path: '',
                component: ActorList,
                name: 'ActorList'
            },
            {
                path: ':id',
                component: ActorShow,
                name: 'ActorShow'
            },
            {
                path: ':id/edit',
                component: ActorEdit,
                name: 'ActorEdit'
            }
        ]
    },
    {
        path: '/users',
        name: 'Users',
        component: Users,
        menuTitle: 'Utenti'
    },
    {
        path: '/users/:id',
        name: 'UserDetail',
        component: User
    },
    {
        path: '/user/:id',
        redirect: '/users/:id'
    }
];
export default authRoutes;
