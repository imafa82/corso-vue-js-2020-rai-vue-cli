import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all'
import 'bootstrap/dist/css/bootstrap.css'
import 'popper.js'
import 'jquery'
import 'bootstrap'
import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import router from './router'
import Card from "./components/Card";
import Header from "./components/Header";
import VueTouch from "vue-touch";
import authInterceptor from "./interceptors/authInterceptor";
import {store} from './store/store'
import testDirective from "./directives/testDirective";
import flexDirective from "./directives/flexDirective";
import flexBasisDirective from "./directives/flexBasisDirective";

Vue.config.productionTip = false;
Vue.use(VueResource);
Vue.http.options.root= "https://jsonplaceholder.typicode.com/";
Vue.http.options.headers = {
  'Content-type': 'application/json'
};
Vue.directive('test', testDirective);
Vue.directive('flex', flexDirective);
Vue.directive('basis', flexBasisDirective);
Vue.filter('toUppercase', function (value) {
  return value.toString().toUpperCase();
});
Vue.filter('toFilterArray', function (value, property, text) {
  return text && property?
      value.filter(el => el[property].toLowerCase().indexOf(text.toLowerCase()) !== -1)
      : value;
});
//Vue.mixin(authService);
// router.beforeEach((to, from, next) => {
//   next();
// });
Vue.http.interceptors.push(authInterceptor);
Vue.use(VueTouch);
Vue.component('app-card', Card);
Vue.component('app-header', Header);
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
