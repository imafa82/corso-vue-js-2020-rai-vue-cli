const NoAuthGuard = (to, from, next) => {
    if(localStorage.getItem('token')){
        next({
            name: 'Home'
        });
    } else {
        next();
    }
}

export default NoAuthGuard;
