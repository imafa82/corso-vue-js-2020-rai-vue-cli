const AuthGuard = (to, from, next) => {
    if(localStorage.getItem('token')){
        next();
    } else {
        next({
           name: 'Login'
        });
    }
}

export default AuthGuard;
