export default {
    bind(el, binding, vnode){
        let cols = !binding.value || isNaN(binding.value.cols)? 12 : parseInt(binding.value.cols);
        let col = !binding.value || isNaN(binding.value.col)? 0 : parseInt(binding.value.col);
        el.style.flexBasis = col && cols? (col/cols * 100)+ '%' : 0;

    }
}
