export default {
    bind(el, binding, vnode){
        console.log(binding);
        if(!binding.modifiers.nowrap){
            el.style.flexWrap = 'wrap';
        }
        if(binding.modifiers.justify){
            el.style.justifyContent = 'center'
        }
        if(binding.modifiers.align){
            el.style.alignItems = 'center'
        }
        if(binding.modifiers.column && !binding.modifiers.reverse){
            el.style.flexDirection = 'column'
        }
        if(binding.modifiers.reverse){
            el.style.flexDirection = binding.modifiers.column ? 'column-reverse' : 'row-reverse'
        }
        el.style.display = 'flex';
    }
}
